#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2009  Felix Rabe  <public@felixrabe.net>
# Licensed under the terms of the GNU General Public License, version 3 or later.

import collections
import datetime
import optparse
import os
import shlex
import sqlite3
import sys
import textwrap

from PyGTKShell.API import *


def automatic_tags():
    t = datetime.datetime.now()
    s = t.strftime("%Y-%m-%dT%H:%M:%S.%f")
    return " #time#%s " % s

def get_automatic_tags(tags):
    res = collections.defaultdict(list)
    for tag in tags.split():
        if tag.startswith("#"):
            i = tag.index("#", 1)
            res[tag[1:i]].append(tag[i+1:])
    if "time" in res:
        time = datetime.datetime.strptime(res["time"][0], "%Y-%m-%dT%H:%M:%S.%f")
        res["time"] = [time]
    return res


## JOT DATABASE

class JotDB(object):

    def __init__(self, filename):
        super(JotDB, self).__init__()
        self.conn = sqlite3.connect(filename)
        self.filename = filename
        self.conn.execute("""create table if not exists jotdb
                             (id integer primary key, note text, tags text)""")
        self.conn.execute("""create table if not exists config
                             (key text primary key, value text)""")
        self.conn.commit()
        
    def enter(self, row_id, note, tags):  # row_id may be None to add a new row
        # print "enter() %r %r %r" % (row_id, note, tags)
        cur = self.conn.cursor()
        tags = " %s " % " ".join(tags.split())
        s = " #tags-of#"
        i = tags.find(s)
        if i != -1 and row_id is not None:
            # print "* removing %u" % row_id
            cur.execute("delete from jotdb where id = ?", (row_id,))
            self.conn.commit()
            tags_note = note
            row_id = int(tags[i+len(s):].split(None, 1)[0])
            row_id, note, tags = self.get_item(row_id)
            if tags_note:  # erase all tags by entering " " (a space)
                tags = tags_note
        else:
            if not note:  # if no note is entered, erase the specified row
                if not row_id:
                    return None
                # print "* removing %u" % row_id
                cur.execute("delete from jotdb where id = ?", (row_id,))
                self.conn.commit()
                return row_id
        # print "* insert/replace %r %r %r" % (row_id, note, tags)
        cur.execute("insert or replace into jotdb values (?, ?, ?)",
                    (row_id, note, tags))
        last_id = cur.lastrowid
        self.conn.commit()
        return last_id
    
    def get_count(self):
        cur = self.conn.cursor()
        cur.execute("select count(*) from jotdb")
        return cur.fetchone()[0]
        
    def get_list(self, tag_filter=None):  # TODO: implement tag filtering
        cur = self.conn.cursor()
        cur.execute("select * from jotdb")
        return cur.fetchall()
        
    def get_item(self, row_id):
        cur = self.conn.cursor()
        if row_id is None:
            cur.execute("select * from jotdb order by id desc limit 1")
        else:
            cur.execute("select * from jotdb where id = ?", (row_id,))
        return cur.fetchone()
    
    def get_config(self, key):
        cur = self.conn.cursor()
        cur.execute("select value from config where key = ?", (key,))
        value = cur.fetchone()
        if value is None: return None
        return value[0]
    
    def set_config(self, key, value):
        cur = self.conn.cursor()
        cur.execute("insert or replace into config values (?, ?)", (key, value))
        self.conn.commit()


## COMMAND EXECUTION LAYER

class CommandExec(object):
    
    # API

    def __init__(self, jotdb):
        super(CommandExec, self).__init__()
        self.jotdb = jotdb
    
    def enter(self, row_id, note, tags):
        if not note.startswith(","):  # not a command
            self._enter(row_id, note, tags)
            return
        command = shlex.split(note[1:])
        try:
            cmd, args = command[0], command[1:]
            cmd_attr = "_cmd_%s" % cmd.lower()
            if not hasattr(self, cmd_attr):
                raise Exception, "Unknown command: %s" % cmd
            getattr(self, cmd_attr)(row_id, tags, *args)
        except Exception, e:
            self._cb_raise_exception(e)
    
    def _enter(self, row_id, note, tags, show_row=False):
        row_id = self.jotdb.enter(row_id, note, tags)
        self._cb_refresh_view()
        if show_row and row_id is not None:
            self._cb_show_row(row_id, show_row)
        return row_id
    
    # Callbacks (ordered alphabetically)
    
    def _cb_quit(self):
        return None
    
    def _cb_raise_exception(self, e):
        note = "## ERROR: %s" % e
        tags = automatic_tags()
        self._enter(None, note, tags, True)
    
    def _cb_refresh_view(self):
        return None
    
    def _cb_show_row(self, row_id, how):
        return None
    
    # Commands (ordered alphabetically, long names first)
    
    def _cmd_append(self, row_id, tags):
        """
        Append to a note.
        """
        row_id, note, tags = self.jotdb.get_item(row_id)
        self._enter(row_id, note, tags, "append")
    _cmd_a = _cmd_append
    
    def _cmd_delete(self, row_id, tags):
        """
        Delete a note.
        """
        row_id, note, tags = self.jotdb.get_item(row_id)
        self._enter(row_id, "", tags)
    _cmd_d = _cmd_delete
    
    def _cmd_edit(self, row_id, tags):
        """
        Edit a note.
        """
        row_id, note, tags = self.jotdb.get_item(row_id)
        self._enter(row_id, note, tags, True)
    _cmd_e = _cmd_edit
    
    def _cmd_filter(self, row_id, tags, cmd, *args):
        """
        List and edit filters.
        
        Type ',filter list' to list all registered filters.
        Type ',filter edit <name>' to edit or create a filter called <name>.
        """
        if "list".startswith(cmd.lower()):
            filters = {}
            for row_id, note, tags in self.jotdb.get_list():
                atags = get_automatic_tags(tags)
                filters[atags.get("filter")] = None
            note = ["List of filters:", ""]
            for name in filters:
                if not name: continue
            note = "\n".join(note) + "\n"
            tags = automatic_tags()
            self._enter(None, note, tags, True)
            return
        if "edit".startswith(cmd.lower()):
            name = args[0]
            for row_id, note, tags in self.jotdb.get_list():
                atags = get_automatic_tags(tags)
                if name in atags["filter"]: break
            else:
                row_id = None
                note = "# Filter: %s\n\n" % name
                tags = automatic_tags() + " #filter#" + name
                row_id = self._enter(row_id, note, tags)
            self._cb_show_row(row_id, "append")
            return
    _cmd_f = _cmd_filter
    
    def _cmd_help(self, row_id, tags, command=None):
        """
        Get help about Jot or about a Jot command.
        
        Type ',help' (comma + help) to get help.
        Type ',help <command>' to get more help about a specific command.
        """
        general_note = textwrap.dedent("""
            Jot Help
            
            With Jot, you can easily jot down notes of all sorts.  To manage
            your notes, you can tag, filter, and sort them.  You can even format
            the metadata information that is displayed to the left of the note
            display.  Both graphical and command line interfaces are very easy
            to work with.
            
            Type ',list' to list all available commands together with a summary.
            Type ',help <command>' to get more help about a specific command.
            
            Hint: To erase a note, make it empty.
            """).strip()
        tags = automatic_tags()
        note = general_note
        if command is not None:
            method_name = "_cmd_%s" % command.lower()
            if not hasattr(self, method_name):
                raise Exception, "Unknown command: %s" % command
            method = getattr(self, method_name)
            note = textwrap.dedent(method.__doc__).strip() + "\n"
        self._enter(None, note, tags, True)
    _cmd_h = _cmd_help

    def _cmd_list(self, row_id, tags):
        """
        List all available commands.
        """
        note = ["List of Commands", ""]
        for names, method in self._command_list:
            note.append(" ".join(",%s" % n for n in names))
            note[-1] += " - " + method.__doc__.strip().split("\n", 1)[0]
        note = "\n".join(note) + "\n"
        tags = automatic_tags()
        self._enter(None, note, tags, True)
    _cmd_l = _cmd_list
    
    def _cmd_multiline(self, row_id, tags):
        """
        Convert a note to a multi-line note by appending a line break.
        """
        row_id, note, tags = self.jotdb.get_item(row_id)
        note += "\n"
        self._enter(row_id, note, tags, "append")
    _cmd_m = _cmd_multiline
    
    def _cmd_new(self, row_id, tags):
        """
        Edit a new note.
        """
        self._enter(None, "", tags)
    _cmd_n = _cmd_new
    
    def _cmd_quit(self, row_id, tags):
        """
        Quit Jot immediately.
        """
        self._cb_quit()
    _cmd_q = _cmd_quit
    
    def _cmd_tag(self, row_id, tags):
        """
        Edit a note's tags.
        
        A new note will be created with a tag that references the original note.
        Tags are separated by whitespace (blanks, end-of-lines, etc.).  Erasing
        the text of the new note will just erase the new note, not remove all
        tags of the referenced note.  To remove all tags of the referenced note,
        enter a space before hitting "Jot".
        """
        row_id, note, tags = self.jotdb.get_item(row_id)
        note = "\n".join(tags.split()) or " "
        tags = automatic_tags() + " #tags-of#%u" % row_id
        self._enter(None, note, tags, "append")
    _cmd_t = _cmd_tag
    
    loc = locals()
    dic = collections.defaultdict(list)
    for k in loc.keys():
        if not k.startswith("_cmd_"): continue
        dic[loc[k]].append(k[5:])
    _command_list = []
    for fn, n in dic.iteritems():
        _command_list.append((sorted(n), fn))
    _command_list.sort()
    del dic, fn, k, loc, n


## GRAPHICAL USER INTERFACE

class CommandExecGUI(CommandExec):

    def __init__(self, main_window):
        super(CommandExecGUI, self).__init__(main_window.jotdb)
        self.main_window = main_window

    def _cb_quit(self):
        self.main_window.save_window_state()
        self.main_window.destroy()

    def _cb_refresh_view(self):
        self.main_window.refresh_jotdb_model()

    def _cb_show_row(self, row_id, how):
        mwin = self.main_window
        sel = mwin.jotdb_view.get_selection()
        for row in mwin.jotdb_model:
            if row[0] == row_id:
                sel.select_path(row.path)
                gobject.idle_add(mwin.jot_widget_grab_focus, how)
                return None
        raise Exception, "invalid row id"

class MainWindow(Window):

    def __init__(self, jotdb, cache_path):
        super(MainWindow, self).__init__()
        self.set_default_size(800, 600)
        self.connect("delete-event", self.delete_event)
        self.connect("window-state-event", self.window_state_event)
        
        self.jotdb = jotdb
        self.cmdexec = CommandExecGUI(self)
        self.set_title("%s - Jot" % os.path.basename(self.jotdb.filename))
        self.cache_path = cache_path
        
        geom = self.jotdb.get_config("window-geometry")
        if geom: self.parse_geometry(geom)
        window_state = int(self.jotdb.get_config("window-state") or "0")
        self.is_maximized = False
        if window_state == 1:
            self.maximize()
            self.is_maximized = True
        
        outer_vbox, inner_vbox = gnome_hig(self)[1:]
        self.paned = gnome_hig(inner_vbox(VPaned(), True, True))
        
        # The model contains these fields:
        # row_id, note, tags, info, note_display
        self.jotdb_model = gtk.ListStore(int, str, str, str, str)
        sw = gnome_hig(self.paned(ScrolledWindow(), True, False))
        self.jotdb_view = gnome_hig(sw(TreeView(self.jotdb_model)))
        self.jotdb_view.connect("row-activated", self.jot_row_activated)
        jot_selection = self.jotdb_view.get_selection()
        jot_selection.connect("changed", self.jot_selection_changed)
        cellr = gtk.CellRendererText()
        
        col = gtk.TreeViewColumn("Info", cellr, text=3)
        col.set_resizable(True)
        width = self.jotdb.get_config("info-column-width")
        if width:
            width = int(width)
            col.set_fixed_width(width)
            col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        self.jotdb_view.append_column(col)
        
        col = gtk.TreeViewColumn("Note", cellr, text=4)
        col.set_resizable(True)
        self.jotdb_view.append_column(col)
        
        jot_box = self.paned(VBox(), False, False)
        self.jot_single = gnome_hig(jot_box(HBox(), False, False))
        self.jot_entry = self.jot_single(Entry(), True, True)
        self.jot_entry.connect("key-press-event", self.jot_key_press)
        self.jot_widget = self.jot_entry
        self.jot_single_button = DefaultButton("_Jot")
        self.jot_single(self.jot_single_button, False, False)
        self.jot_single_button.connect("clicked", self.jot_clicked)
        
        self.jot_multi = gnome_hig(jot_box(VBox(auto_show=False), True, True))
        sw = gnome_hig(self.jot_multi(ScrolledWindow(), True, True))
        self.jot_textview = gnome_hig(sw(TextViewForCode()))
        self.jot_textview.connect("key-press-event", self.jot_key_press)
        self.jot_multi_button = DefaultButton("_Jot", auto_default=False)
        self.jot_multi(self.jot_multi_button, False, False)
        self.jot_multi_button.connect("clicked", self.jot_clicked)
        
        gobject.idle_add(self.jot_widget_grab_focus)
        self.jot_single.paned_position = 0
        pos = self.jotdb.get_config("single-paned-position")
        if pos:
            self.jot_single.paned_position = int(pos)
            self.paned_set_position(int(pos))
        self.jot_multi.paned_position = 300
        pos = self.jotdb.get_config("multi-paned-position")
        if pos:
            self.jot_multi.paned_position = int(pos)
        
        self.refresh_jotdb_model()
    
    # Window and Paned state management
    
    def delete_event(self, window, event):
        self.save_window_state()
        return False
    
    def save_window_state(self):
        geom = "%ux%u+%u+%u" % (self.get_size() + self.get_position())
        self.jotdb.set_config("window-geometry", geom)
        window_state = int(self.is_maximized)
        self.jotdb.set_config("window-state", str(window_state))
        width = self.jotdb_view.get_column(0).get_width()
        self.jotdb.set_config("info-column-width", str(width))
        pos = self.jot_single.paned_position
        self.jotdb.set_config("single-paned-position", str(pos))
        pos = self.jot_multi.paned_position
        self.jotdb.set_config("multi-paned-position", str(pos))
    
    def window_state_event(self, window, event):
        if event.changed_mask & gtk.gdk.WINDOW_STATE_MAXIMIZED:
            self.is_maximized = bool(event.new_window_state &
                                     gtk.gdk.WINDOW_STATE_MAXIMIZED)
        return False
    
    def paned_get_position(self):
        return self.get_size()[1] - self.paned.get_position()
    
    def paned_set_position(self, pos):
        self.paned.set_position(self.get_size()[1] - pos)
    
    # Jot single / multi / widget
    
    def set_jot_is_multiline(self, is_multiline):
        single_visible = self.jot_single.get_property("visible")
        if not (is_multiline ^ single_visible):
            if not is_multiline:  # single-line
                self.jot_multi.paned_position = self.paned_get_position()
                self.jot_multi.hide()
                self.jot_single.show()
                self.jot_widget = self.jot_entry
                self.paned_set_position(self.jot_single.paned_position)
                self.jot_single_button.grab_default()
            else:  # multi-line
                self.jot_single.paned_position = self.paned_get_position()
                self.jot_single.hide()
                self.jot_multi.show()
                self.jot_widget = self.jot_textview
                self.paned_set_position(self.jot_multi.paned_position)
                self.jot_multi_button.grab_default()
        return is_multiline
    
    def jot_widget_grab_focus(self, how=True):
        self.jot_widget.grab_focus()
        if self.jot_single.get_property("visible"):  # single-line
            if how == "append":
                self.jot_widget.set_position(-1)
        else:  # multi-line
            buf = self.jot_widget.get_buffer()
            start, end = buf.get_start_iter(), buf.get_end_iter()
            if how == "append":
                buf.select_range(end, end)
            else:
                buf.select_range(start, end)
            self.jot_widget.scroll_to_iter(buf.get_end_iter(), 0.0)
    
    def jot_key_press(self, widget, event):
        if KeyPressEval("CONTROL, Return")(event):
            self.jot_single_button.clicked()
            return True
        return False
    
    def jot_widget_get_text(self):
        if self.jot_single.get_property("visible"):  # single-line
            return self.jot_entry.get_text()
        else:  # multi-line
            return self.jot_textview.get_buffer()()
    
    def jot_widget_set_text(self, text):
        if not self.set_jot_is_multiline("\n" in text):  # single-line
            self.jot_entry.set_text(text)
        else:  # multi-line
            self.jot_textview.get_buffer().set_text(text)
    
    # Jot button
    
    def jot_clicked(self, button):
        if self.row_id is None:
            tags = automatic_tags()
        else:
            row_id, note, tags = self.jotdb.get_item(self.row_id)
        note = self.jot_widget_get_text()
        self.jot_widget_set_text("")
        self.cmdexec.enter(self.row_id, note, tags)
    
    # Jot DB model and view
    
    def jot_row_activated(self, treeview, path, view_column):
        self.jot_widget_grab_focus()
    
    def jot_selection_changed(self, jot_selection):
        it = jot_selection.get_selected()[1]
        if it is None:
            self.row_id = None
        else:
            row = self.jotdb_model[it]
            self.row_id = row[0]
        note = ""
        if self.row_id is not None:
            note = row[1]
        self.jot_widget_set_text(note)
        if it is None:
            gobject.idle_add(self.jot_widget_grab_focus)
        return False
    
    def refresh_jotdb_model(self):
        self.row_id = None
        self.jotdb_model.clear()
        for row_id, note, tags in self.jotdb.get_list():
            atags = get_automatic_tags(tags)
            info = (atags.get("time") or [""])[0]
            note_display = note
            if "\n" in note:
                note_display = "[+] %s" % note.split("\n", 1)[0]
            self.jotdb_model.append((row_id, note, tags, info, note_display))
        if len(self.jotdb_model):
            self.jotdb_view.scroll_to_cell(self.jotdb_model[-1].path)
    

## COMMAND LINE INTERFACE

class CommandExecCLI(CommandExec):

    pass


class OptionParser(optparse.OptionParser):

    def __init__(self, *a, **kw):
        optparse.OptionParser.__init__(self, *a, **kw)
        self.add_option("-f", "--jot-file",
                        help="Jot file file to use")
        self.add_option("-F", "--file-chooser", action="store_true",
                        help="Show file chooser dialog to select Jot file")


def main(argv):
    option_parser = OptionParser()
    options, args = option_parser.parse_args(argv[1:])
    
    cache = os.environ.get("XDG_CACHE_HOME", os.path.expanduser("~/.cache"))
    cache = os.path.join(cache, "jot")
    if not os.path.isdir(cache):
        os.makedirs(cache, 0700)
    filename_cache = os.path.join(cache, "last-file.txt")
    
    filename = options.jot_file
    if not filename and os.path.isfile(filename_cache):
        filename = file(filename_cache).readline().rstrip()
    if not options.jot_file:
        if options.file_chooser or not (filename and os.path.isfile(filename)):
            fcd = FileChooserDialogStandard()
            if filename: fcd.set_filename(filename)
            if fcd.run() != gtk.RESPONSE_ACCEPT:
                raise SystemExit, 0
            filename = fcd.get_filename()
    filename = os.path.abspath(filename)
    
    jotdb = JotDB(filename)
    print >>file(filename_cache, "w"), filename
    
    cmdexec = CommandExecCLI(jotdb)
    if not jotdb.get_count() and not jotdb.get_config("help-hint-added"):
        jotdb.set_config("help-hint-added", True)
        note = "Type ',help' (comma + help) to get help"
        tags = automatic_tags()
        jotdb.enter(None, note, tags)
    
    if args:  # CLI
        note = " ".join(args)
        tags = automatic_tags()
        cmdexec.enter(None, note, tags)
    else:  # GUI
        MainWindow(jotdb, cache)
        main_loop_run()


if __name__ == "__main__":
    main(sys.argv)

